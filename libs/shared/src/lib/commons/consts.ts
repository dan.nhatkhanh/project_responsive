export const SYSTEM_NAME = 'Marketplace';
export const PRIVATE_SALE_ADDRESS = '0x5eeb28b5aef44b6664b342d23b1aadce84196386';
export const COUNT_DOWN_PRIVATE_1 = new Date('Dec 25, 2021 00:00:00').getTime();
export const STATUSNAME: any = {
  'hot_deal': 'Hot deals',
  'new': 'New'
};
export const VIEW_COUNT_HOT_TAG = 500;
