export interface UserCredentials {
  account: string;
  nonce: string;
}
