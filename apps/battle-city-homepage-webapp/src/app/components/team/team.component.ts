/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'wol-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
})
export class TeamComponent implements OnInit {
  products = [
    {
      image: 'assets/images/Heavy_1.png',
      name: 'NAM BUI',
      job: 'CEO'
    },
    {
      image: 'assets/images/Heavy_3.png',
      name: 'PHUC LAM',
      job: 'CTO'
    },
    {
      image: 'assets/images/Main_1.png',
      name: 'ANH TRAN',
      job: 'Blockchain Expert'
    },
    {
      image: 'assets/images/Main-01.png',
      name: 'CUONG LE',
      job: 'Business Development & Community Manager'
    },
    {
      image: 'assets/images/Main-02.png',
      name: 'DU NGUYEN',
      job: 'Partnership Manager'
    },
    {
      image: 'assets/images/Main-03.png',
      name: 'NGHIA LE',
      job: 'Legal Counsel'
    },
    {
      image: 'assets/images/Scout-01.png',
      name: 'TAN DUONG',
      job: 'Art Director'
    },
    {
      image: 'assets/images/Scout-02.png',
      name: 'SONY HUYNH',
      job: 'DevSecOps Engineer'
    },
    {
      image: 'assets/images/Heavy_2.png',
      name: 'DUY NGUYEN',
      job: 'Unity Developer'
    },
    {
      image: 'assets/images/Scout-03.png',
      name: 'TRUONG DAN',
      job: 'Software Engineer'
    },
    {
      image: 'assets/images/DEP.png',
      name: 'HAO TRAN',
      job: 'Unity Developer'
    }
  ];
  responsiveOptions = [
    {
      breakpoint: '1400px',
      numVisible: 3,
      numScroll: 3,
    },
    {
      breakpoint: '1100px',
      numVisible: 2,
      numScroll: 2,
    },
    {
      breakpoint: '576px',
      numVisible: 2,
      numScroll: 2,
    },
  ];

  ngOnInit(): void {
    const items = document.querySelectorAll('.carousel .carousel-item');
    items.forEach((element) => {
      const minPerSlide = 4;
      let next = element.nextElementSibling;
      for (let i = 1; i < minPerSlide; i++) {
        if (!next) {
          next = items[0];
        }
        const cloneChild = next.cloneNode(true);
        element.appendChild((cloneChild as any).children[0]);
        next = next.nextElementSibling;
      }
    });
  }
}
