import { Component, Input } from '@angular/core';

@Component({
  selector: 'wol-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.scss'],
})
export class BackgroundComponent {
  @Input() type = 1;
}
