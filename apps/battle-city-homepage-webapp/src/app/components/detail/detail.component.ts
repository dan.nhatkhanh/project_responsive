import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import * as copy from 'copy-to-clipboard';
import { LINK } from '@wol/shared';
import { TranslateService } from '@wol/ui';
import { CoinService } from '../../services/coin.service';

@Component({
  selector: 'wol-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  MU_OF_HEROES = LINK.MU_OF_HEROES;
  MAIN_ADDRESS = environment.COMMON.MAIN_ADDRESS || '';
  TOKEN = 'WOL';
  PRICE = 'N/A';
  TOTAL_SUPPLY = '100,000,000';
  MARKET_CAP = 'N/A';
  BNB_PRICE = 'N/A';
  LP_HOLDINGS = 'N/A';
  currentPosition = 0;
  heroes = [
    `nft-1.svg`,
    `nft-2.svg`,
    `nft-3.svg`,
    `nft-4.svg`,
  ]

  constructor(
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private coinService: CoinService
  ) { }

  ngOnInit(): void {
    this.autoSwipped();
    this.autoUpdate()
    // this.currentPosition = this.randomPosition();
  }

  autoUpdate() {
    this.updateCoinInfo();
    
    let interval = setInterval(() => {
      this.updateCoinInfo();
    }, 50 * 1000)
  }

  updateCoinInfo() {
    this.coinService.getCoinInfo().then((r: any) => {
      const wolInfo = r[0];
      this.PRICE = '$' + wolInfo?.current_price;
      this.MARKET_CAP = wolInfo?.market_cap;
    });

    this.coinService.getCoinPriceInBNB().then((r : any) => {
      this.BNB_PRICE = '$' + r[this.MAIN_ADDRESS]?.bnb;
    })
  }

  randomPosition() {
    return Math.floor(Math.random() * (this.heroes.length - 0)) + 0;
  }

  herf() {
    location.href = this.MU_OF_HEROES;
  }

  autoSwipped() {
    let interval = setInterval(() => {
      this.next();
    }, 5 * 1000)
  }

  next() {

    if (this.currentPosition + 1 > this.heroes.length - 1) {
      this.currentPosition = 0;
    }
    else {
      this.currentPosition += 1;
    }

  }
  previous() {

    if (this.currentPosition - 1 < 0) {
      this.currentPosition = this.heroes.length - 1;
    }
    else {
      this.currentPosition -= 1;
    }
    console.log(this.currentPosition);
  }

  copyAddress() {
    copy(this.MAIN_ADDRESS);
    this.toastrService.success(this.translateService.translate('messages.copied'));
  }

}
