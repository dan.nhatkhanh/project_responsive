import { AfterViewInit, Component, OnInit } from "@angular/core";
import { LINK } from "@wol/shared";

@Component({
  selector: "wol-introduce",
  templateUrl: "./introduce.component.html",
  styleUrls: ["./introduce.component.scss"],
})
export class IntroduceComponent implements AfterViewInit {
  segments = [
    {
      id: 1,
      text: `we are [Diversifying]`,
      bold: `web3 gamers`,
    },
    {
      id: 2,
      text: `we are [creating]`,
      bold: `[web3 game for anyone]`,
    },
    {
      id: 3,
      text: `we are [evelating]`,
      bold: `[web3 gaming]`,
    },
    {
      id: 4,
      text: `we are [building]`,
      bold: `[all in one platform for gamers]`,
    },
  ];

  renderSegment: any = this.segments[0];

  WHITEPAPER = LINK.WHITEPAPER;

  PORTAL = LINK.PORTAL;

  constructor() {}

  ngAfterViewInit(): void {
    const segments = [
      {
        id: 1,
        text: `we are [Diversifying]`,
        bold: `web3 gamers`,
      },
      {
        id: 2,
        text: `we are [creating]`,
        bold: `[web3 game for anyone]`,
      },
      {
        id: 3,
        text: `we are [elevating]`,
        bold: `[web3 gaming]`,
      },
      {
        id: 4,
        text: `we are [building]`,
        bold: `[all in one platform for gamers]`,
      },
    ];

    const textDisplay = document.getElementById("text-display");
    let segmentIndex = 0;
    let charIndex = 0;
    let isDeleting = false;

    // Calculate the maximum width and height needed
    function calculateDimensions() {
      let maxWidth = 0;
      let maxHeight = 0;

      segments.forEach((segment) => {
        const tempElement = document.createElement("p");
        tempElement.style.fontSize = "24px";
        tempElement.style.fontWeight = "bold";
        tempElement.style.position = "absolute";
        tempElement.style.visibility = "hidden";
        tempElement.textContent = segment.bold;
        document.body.appendChild(tempElement);

        const { width, height } = tempElement.getBoundingClientRect();
        if (width > maxWidth) maxWidth = width;
        if (height > maxHeight) maxHeight = height;

        document.body.removeChild(tempElement);
      });

      return { maxWidth, maxHeight };
    }

    // Set the container and text-display dimensions
    function setDimensions() {
      const { maxHeight } = calculateDimensions();

      const container = document.getElementById("text-display-wrapper");
      (container as HTMLElement).style.width = `100%`;
      (container as HTMLElement).style.height = `${maxHeight}px`;

      (textDisplay as HTMLElement).style.width = "100%";
      (textDisplay as HTMLElement).style.height = `${maxHeight}px`;
      (textDisplay as HTMLElement).style.lineHeight = `${maxHeight}px`;
    }

    function typeEffect() {
      const currentSegment = segments[segmentIndex];
      const currentText = currentSegment.bold;

      if (isDeleting) {
        if (charIndex > 0) {
          charIndex--;
          (textDisplay as HTMLElement).textContent = currentText.substring(
            0,
            charIndex
          );
        } else {
          isDeleting = false;
          segmentIndex = (segmentIndex + 1) % segments.length;
        }
      } else {
        if (charIndex < currentText.length) {
          charIndex++;
          (textDisplay as HTMLElement).textContent = currentText.substring(
            0,
            charIndex
          );
        } else {
          isDeleting = true;
          setTimeout(typeEffect, 1000); // Pause before starting to delete
          return;
        }
      }

      const speed = isDeleting ? 50 : 100; // Adjust speed here
      setTimeout(typeEffect, speed);
    }

    document.addEventListener("DOMContentLoaded", () => {
      setDimensions();
      typeEffect();
    });
  }

  // ngOnInit(): void {
  //   this.handleShowVision();
  // }

  // handleShowVision() {
  //   setInterval(() => {
  //     this.waitText();
  //   }, 6000);
  // }

  // async waitText() {
  //   const evalutedIndex = this.renderSegment.id < 4 ? this.renderSegment.id : 0;
  //   this.renderSegment = {
  //     ...this.segments[evalutedIndex],
  //     id: 0,
  //   };
  //   await this.sleep();
  //   this.renderSegment = this.segments[evalutedIndex];
  // }

  // async sleep() {
  //   return new Promise((resolve) => {
  //     setTimeout(() => {
  //       resolve(true);
  //     }, 800);
  //   });
  // }

  leadTo(url: string) {
    window.open(url);
  }
}
