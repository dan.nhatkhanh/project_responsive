import { Component, OnInit } from '@angular/core';
import { LINK } from '@wol/shared';
import { slideInLeft, slideInRight } from './animation';
@Component({
    selector: 'wol-image-slider',
    templateUrl: './image-slider.component.html',
    styleUrls: ['./image-slider.component.scss'],
    animations: [slideInLeft, slideInRight]
})
export class ImageSliderComponent implements OnInit {
    counter: number = 0;
    show: boolean = true;
    AWS = LINK.AWS;
    BINANCE = LINK.BINANCE;
    ANTS = LINK.ANTS;
    BLG = LINK.BLG;
    HACKEN = LINK.HACKEN;
    UNICRYPT = LINK.UNICRYPT;
    BITKEEP = LINK.BITKEEP;
    SOLANA = LINK.SOLANA;
    POLYGON = LINK.POLYGON;
    DAPP = LINK.DAPP;
    DAPPRADAR = LINK.DAPPRADAR;
    COINMARKETCAP = LINK.COINMARKETCAP;
    MYRIA = LINK.MYRIA;
    currentIndex = 0;
    images: any[] = [
        [
            {
                link: this.SOLANA,
                icon: 'assets/images/solanaLogo.svg'
            },
            {
                link: this.BINANCE,
                icon: 'assets/images/binance.png'
            },
            {
                link: this.POLYGON,
                icon: 'assets/images/polygonLogo4.png',
                height: '6.8125rem'
            },
            {
                link: this.AWS,
                icon: 'assets/images/aws.png'
            },

        ],
        [
            {
                link: this.HACKEN,
                icon: 'assets/images/hacken.png'
            },

            {
                link: this.BITKEEP,
                icon: 'assets/images/bitkeep.png'
            },
            {
                link: this.MYRIA,
                icon: 'assets/images/myriaLogo2.png'
            },
            {
                link: this.DAPPRADAR,
                icon: 'assets/images/dappLogo.png'
            }
        ],
        [

            {
                link: this.DAPP,
                icon: 'assets/images/dappradarLogo.svg'
            },
            {
                link: this.COINMARKETCAP,
                icon: 'assets/images/cmcLogo.jpeg'
            },
            // {
            //     link: this.ANTS,
            //     icon: 'assets/images/nts.png'
            // },
            {
                link: this.BLG,
                icon: 'assets/images/blg.png'
            },


        ],
        [
            {
                link: LINK.TRUTS,
                icon: 'assets/images/trust.svg'
            },
            {
                link: LINK.PYME,
                icon: 'assets/images/pyme.svg'
            },
            {
                link: LINK.COINPAGE,
                icon: 'assets/images/coinpage.png'
            },
            {
                link: LINK.BBCRYPTO,
                icon: 'assets/images/bb.jpeg'
            },

        ],
        [{
            link: LINK.SOLAPP_SOLWORK,
            icon: 'assets/images/sollapps.jpeg',

        },
        {
            link: LINK.XPLUS,
            icon: 'assets/images/xplus.png'
        },
        {
            link: LINK.AFFLUX,
            icon: 'assets/images/afflux.jpeg'
        },
        {
            link: LINK.DENET,
            icon: 'assets/images/denet.png'
        },],
        [
            {
                link: LINK.BITMART,
                icon: 'assets/images/bitmart.png',

            },
            {
                link: LINK.CASTRUM,
                icon: 'assets/images/castrum.jpeg'
            },
            {
                link: LINK.CHAINPEAK,
                icon: 'assets/images/chainpeak.png'
            },


            {
                link: LINK.CRYPTOMASTER,
                icon: 'assets/images/cryptomaster.jpeg'
            },

        ],
        [
            {
                link: LINK.DEBOX,
                icon: 'assets/images/debox.png'
            },
            {
                link: LINK.POAPTHON,
                icon: 'assets/images/poapthon.png'
            },
            {
                link: LINK.MAGIC_SQUARE,
                icon: 'assets/images/magicsquare.png'
            },
            {
                link: LINK.METAFIGHT,
                icon: 'assets/images/metafight.jpeg'
            },

        ],
        [
            {
                link: LINK.CEBG,
                icon: 'assets/images/cebg.jpeg'
            },
            {
                link: LINK.FANDORA,
                icon: 'assets/images/logo.svg'
            },
            {
                link: LINK.CRE8R,
                icon: 'assets/images/cre8r.png'
            },
            {
                link: LINK.KIMA,
                icon: 'assets/images/kima.png'
            },

        ],
        [
            {
                link: LINK.REI_NETWORK,
                icon: 'assets/images/reinetwork.svg'
            },
            {
                link: LINK.TASK_ON,
                icon: 'assets/images/taskon.png'
            },
            {
                link: LINK.ETH_GAME,
                icon: 'assets/images/ethgame.png'
            },
            {
                link: LINK.TRANTOR,
                icon: 'assets/images/trantor.png'
            },

        ],
        [
            {
                link: LINK.GALAXY_BLITZ,
                icon: 'assets/images/galaxyblitz.jpeg'
            },
            {
                link: LINK.COIN_GECKO,
                icon: 'assets/images/cg.svg'
            },
        ]
    ];
    partnersMobile: any[] = [];
    isMobile = false;
    constructor() {
        this.partnersMobile = this.images.flat();
        if (window.innerWidth <= 500) {
            this.isMobile = true;
            this.images = this.partnersMobile;
        }
        this.autoSwipped();
    }
    openLink(url: string) {
        window.open(url);
    }
    autoSwipped() {
        let interval = setInterval(() => {
            this.next();
        }, 5 * 1000)
    }

    next() {

        if (this.currentIndex + 1 > this.images.length - 1) {
            this.currentIndex = 0;
        }
        else {
            this.currentIndex += 1;
        }

    }
    previous() {

        if (this.currentIndex - 1 < 0) {
            this.currentIndex = this.images.length - 1;
        }
        else {
            this.currentIndex -= 1;
        }
    }

    getLeft() {
        if (this.currentIndex > 0) {
            this.currentIndex -= 1;
            return this.currentIndex
        }
        return 0;
    }

    getRight() {
        if (this.currentIndex < this.images.length - 1) {
            this.currentIndex += 1;
            return this.currentIndex;
        }
        return this.images.length - 1;
    }

    ngOnInit() {
    }

    onNext() {
        if (this.counter != this.images.length - 1) {
            this.counter++;
        }
    }

    onPrevious() {
        if (this.counter > 0) {
            this.counter--;
        }
    }

}