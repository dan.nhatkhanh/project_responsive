import { Component, OnInit } from '@angular/core';
import { LINK } from '@wol/shared';
import { dataPartner } from './data';

@Component({
  selector: 'wol-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss']
})
export class PartnerComponent implements OnInit {
  AWS = LINK.AWS;
  BINANCE = LINK.BINANCE;
  ANTS = LINK.ANTS;
  BLG = LINK.BLG;
  HACKEN = LINK.HACKEN;
  UNICRYPT = LINK.UNICRYPT;
  BITKEEP = LINK.BITKEEP;
  SOLANA = LINK.SOLANA;
  POLYGON = LINK.POLYGON;
  DAPP = LINK.DAPP;
  DAPPRADAR = LINK.DAPPRADAR;
  COINMARKETCAP = LINK.COINMARKETCAP;
  MYRIA = LINK.MYRIA;
  LINE1: any[] = [
  ]

  LINE2: any[] = [
  ]

  LINE3: any[] = [
  ]

  LINE4: any[] = [
  ]

  LINE5: any[] = [

  ]

  LINE6: any[] = []
  LINE7: any[] = []
  partnerData = dataPartner;

  getClassPartner(length: number) {
    const target = 12 / length
    return `col-md-${target} col partner`
  }

  ngOnInit(): void {
    this.LINE1 = [
      {
        link: this.SOLANA,
        icon: 'assets/images/solanaLogo.svg'
      },
      {
        link: this.BINANCE,
        icon: 'assets/images/binance.png'
      },
      {
        link: this.POLYGON,
        icon: 'assets/images/polygonLogo4.png',
        height: '6.8125rem'
      },
      {
        link: this.AWS,
        icon: 'assets/images/aws.png'
      },
    ]

    this.LINE2 = [
      {
        link: this.HACKEN,
        icon: 'assets/images/hacken.png'
      },
      {
        link: this.BITKEEP,
        icon: 'assets/images/bitkeep.png'
      },
      {
        link: this.MYRIA,
        icon: 'assets/images/myriaLogo2.png'
      },
      {
        link: this.DAPPRADAR,
        icon: 'assets/images/dappLogo.png'
      },
      {
        link: this.DAPP,
        icon: 'assets/images/dappradarLogo.svg'
      },
      {
        link: this.COINMARKETCAP,
        icon: 'assets/images/cmcLogo.jpeg'
      },
    ];
    this.LINE3 = [
      {
        link: this.ANTS,
        icon: 'assets/images/nts.png'
      },
      {
        link: this.BLG,
        icon: 'assets/images/blg.png'
      },
      {
        link: LINK.TRUTS,
        icon: 'assets/images/trust.svg'
      },
      {
        link: LINK.PYME,
        icon: 'assets/images/pyme.svg'
      },
      {
        link: LINK.COINPAGE,
        icon: 'assets/images/coinpage.png'
      },
      {
        link: LINK.BBCRYPTO,
        icon: 'assets/images/bb.jpeg'
      },
      {
        link: LINK.SOLAPP_SOLWORK,
        icon: 'assets/images/sollapps.jpeg',
        height: '6rem'
      },
      {
        link: LINK.XPLUS,
        icon: 'assets/images/xplus.png'
      },
      {
        link: LINK.AFFLUX,
        icon: 'assets/images/afflux.jpeg'
      },
      {
        link: LINK.DENET,
        icon: 'assets/images/denet.png'
      },
    ];

    this.LINE4 = [

      {
        link: LINK.BITMART,
        icon: 'assets/images/bitmart.png'
      },
      {
        link: LINK.CASTRUM,
        icon: 'assets/images/castrum.jpeg'
      },
      {
        link: LINK.CHAINPEAK,
        icon: 'assets/images/chainpeak.png'
      },


      {
        link: LINK.CRYPTOMASTER,
        icon: 'assets/images/cryptomaster.jpeg'
      },
      

    ]

    this.LINE5 = [

      {
        link: LINK.DEBOX,
        icon: 'assets/images/debox.png'
      },

      {
        link: LINK.POAPTHON,
        icon: 'assets/images/poapthon.png'
      },
      {
        link: LINK.MAGIC_SQUARE,
        icon: 'assets/images/magicsquare.png'
      },
      {
        link: LINK.METAFIGHT,
        icon: 'assets/images/metafight.jpeg'
      },
      
    ]
    this.LINE6 = [
      {
        link: LINK.CEBG,
        icon: 'assets/images/cebg.jpeg'
      },
      {
        link: LINK.FANDORA,
        icon: 'assets/images/logo.svg'
      },
      {
        link: LINK.CRE8R,
        icon: 'assets/images/cre8r.png'
      },
      {
        link: LINK.KIMA,
        icon: 'assets/images/kima.png'
      },
      
    ];

    this.LINE7 = [
      {
        link: LINK.REI_NETWORK,
        icon: 'assets/images/reinetwork.svg'
      },
      {
        link: LINK.TASK_ON,
        icon: 'assets/images/taskon.png'
      },
      {
        link: LINK.ETH_GAME,
        icon: 'assets/images/ethgame.png'
      },
      {
        link: LINK.TRANTOR,
        icon: 'assets/images/trantor.png'
      },
      {
        link: LINK.GALAXY_BLITZ,
        icon: 'assets/images/galaxyblitz.jpeg'
      },
    ]
  }
}
