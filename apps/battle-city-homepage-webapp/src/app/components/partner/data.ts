export const dataPartner = [
    {
        name: 'BNB Chain',
        img: 'https://pbs.twimg.com/profile_images/1565354861616832513/ovh5FyDN.png'
    },
    {
        name: 'Polygon',
        img: 'https://pbs.twimg.com/profile_images/1639270979288399876/3OT3Lsux.jpg'
    },
    {
        name: 'Gate NFT',
        img: 'https://pbs.twimg.com/profile_images/1659595828208746496/PfYHOqud.jpg'
    },
    {
        name: 'Bitmart',
        img: 'https://pbs.twimg.com/profile_images/1652121205846016001/aKTyZSKP.jpg'
    },
    {
        name: 'OKX Web3 (NFT & Wallet & DeFi)',
        img: 'https://pbs.twimg.com/profile_images/1654186051911696389/uAIyoH4t.jpg'
    },
    {
        name: 'SPACE ID',
        img: 'https://pbs.twimg.com/profile_images/1638127852204986368/XtSvEW6D.jpg'
    },
    {
        name: 'Myria | The web3 gaming platform',
        img: 'https://pbs.twimg.com/profile_images/1480412345780293638/Jwxbv8OI.png'
    },
    {
        name: 'Hacken🇺🇦',
        img: 'https://pbs.twimg.com/profile_images/1616106116055711747/B4KGKcVB.jpg'
    },
    {
        name: 'Amazon Web Services',
        img: 'https://pbs.twimg.com/profile_images/1641476962362302464/K8lb6OtN.jpg'
    },
    {
        name: 'Magic Square',
        img: 'https://pbs.twimg.com/profile_images/1609890739630080000/GINHYFc5.png'
    },
    {
        name: 'MetaMirror',
        img: 'https://pbs.twimg.com/profile_images/1630500104804397056/_7B1TEX5.jpg'
    },
    {
        name: 'DeBox',
        img: 'https://pbs.twimg.com/profile_images/1558459716161851392/KC22gLOG.jpg'
    }

    ,
    {
        name: 'TaskOn - A Web3 Task Collaboration Platform',
        img: 'https://pbs.twimg.com/profile_images/1664161319397003269/TM9aYYis.png'
    },
    {
        name: 'PlanckX: GameFi Platform',
        img: 'https://pbs.twimg.com/profile_images/1610544850176462848/e6obkN53.jpg'
    },
    {
        name: 'DeNet',
        img: 'https://pbs.twimg.com/profile_images/1520016441550204928/b4m1B84Q.jpg'
    },
    {
        name: 'Truts',
        img: 'https://pbs.twimg.com/profile_images/1610628469108641793/d-3HAQF5.jpg'
    },
    {
        name: '₿SClaunch',
        img: 'https://pbs.twimg.com/profile_images/1624763072115281921/PpX4QRlu.jpg'
    }

    ,
    {
        name: 'GameTrade.market',
        img: 'https://pbs.twimg.com/profile_images/1515412869818552323/oQJROqeu.png'
    },
    {
        name: 'Galaxy Blitz',
        img: 'https://pbs.twimg.com/profile_images/1605966588322410496/5lSZoE4j.jpg'
    },
    {
        name: 'XPLUS',
        img: 'https://pbs.twimg.com/profile_images/1645760678966489093/H0xxhktn.jpg'
    },
    {
        name: 'AFKDAO',
        img: 'https://pbs.twimg.com/profile_images/1611228725902991362/1lab2FiV.jpg'
    },
    {
        name: 'Crypto News (CoinGape)',
        img: 'https://pbs.twimg.com/profile_images/1515280697468112898/fbw5bH_J.jpg'
    }

    ,
    {
        name: 'WEB3 Space',
        img: 'https://pbs.twimg.com/profile_images/1649043461549740034/wk98Yn22.jpg'
    },
    {
        name: 'VCGamers - Digital Marketplace Platform',
        img: 'https://pbs.twimg.com/profile_images/1557225685881483265/YUe-yZ4W.jpg'
    },
    {
        name: 'GameBoy: GameFi Aggregator Platform',
        img: 'https://pbs.twimg.com/profile_images/1634479510069596160/OUso1fTP.jpg'
    },
    {
        name: 'BEATGEN',
        img: 'https://pbs.twimg.com/profile_images/1638060709006413825/pmyq_q5M.jpg'
    },
    {
        name: 'Mojo Gaming Guild',
        img: 'https://pbs.twimg.com/profile_images/1524983009962622976/CfyoY04Q.jpg'
    }

    ,
    {
        name: 'Passion Guild - Web 3.0 Lovers',
        img: 'https://pbs.twimg.com/profile_images/1514495970590953475/-U87PMoI.jpg'
    },
    {
        name: 'GGslayer',
        img: 'https://pbs.twimg.com/profile_images/1626156972558139392/OBErKrD-.jpg'
    },
    {
        name: 'REI Network｜From #GXChain!',
        img: 'https://pbs.twimg.com/profile_images/1473602564184842243/c51_j0aB.jpg'
    },
    {
        name: 'Fandora Network 🟩',
        img: 'https://pbs.twimg.com/profile_images/1572446176619950081/vIiLQbHa.jpg'
    },
    {
        name: 'DappRadar',
        img: 'https://pbs.twimg.com/profile_images/1551863971703046146/fl3RyAXc.jpg'
    }

    ,
    {
        name: 'Dapp.com',
        img: 'https://pbs.twimg.com/profile_images/1002503691188912128/tfKbuEjQ.jpg'
    },
    {
        name: 'CoinMarketCap',
        img: 'https://pbs.twimg.com/profile_images/1609596934557429760/lIbH-TCC.jpg'
    },
    {
        name: 'CoinGecko',
        img: 'https://pbs.twimg.com/profile_images/1604796574415978496/SIffz3a6.jpg'
    },
    {
        name: 'Counter Fire (ex CEBG_GAME)',
        img: 'https://pbs.twimg.com/profile_images/1671068721215856640/ey3CzTCo.jpg'
    },
    {
        name: 'ETH.game 🎮',
        img: 'https://pbs.twimg.com/profile_images/1623615350586871808/z5bL1k9-.jpg'
    }

    ,
    {
        name: 'Dapp.com',
        img: 'https://pbs.twimg.com/profile_images/1002503691188912128/tfKbuEjQ.jpg'
    },
    {
        name: 'CoinMarketCap',
        img: 'https://pbs.twimg.com/profile_images/1609596934557429760/lIbH-TCC.jpg'
    },
    {
        name: 'CoinGecko',
        img: 'https://pbs.twimg.com/profile_images/1604796574415978496/SIffz3a6.jpg'
    },
    {
        name: 'Counter Fire (ex CEBG_GAME)',
        img: 'https://pbs.twimg.com/profile_images/1671068721215856640/ey3CzTCo.jpg'
    },
    {
        name: 'ETH.game 🎮',
        img: 'https://pbs.twimg.com/profile_images/1623615350586871808/z5bL1k9-.jpg'
    }

    ,
    {
        name: 'CRE8R DAO Prompt Writers Association (CDPWA)',
        img: 'https://pbs.twimg.com/profile_images/1586304857446572032/d1oPAEDo.jpg'
    },
    {
        name: 'Puffverse',
        img: 'https://pbs.twimg.com/profile_images/1607686450044362757/FwEisfSh.jpg'
    },
    {
        name: 'POAPathon',
        img: 'https://pbs.twimg.com/profile_images/1503151899050229763/QuBdfoB6.jpg'
    },
    {
        name: 'SolWorks 🐧',
        img: 'https://pbs.twimg.com/profile_images/1550618425826983938/JnOU91Ll.jpg'
    },
    {
        name: 'ETH.game 🎮',
        img: 'https://pbs.twimg.com/profile_images/1618640804238561283/O8_ecn9_.png'
    }

    ,
    {
        name: 'The Reading Ape | Podcasts to Notes',
        img: 'https://pbs.twimg.com/profile_images/1563702554411687942/LMCDTMeU.jpg'
    },
    {
        name: 'KingdomX',
        img: 'https://pbs.twimg.com/profile_images/1608390596779192321/re64p6c0.png'
    },
    {
        name: 'TAGInk (Formerly TAGuild)',
        img: 'https://pbs.twimg.com/profile_images/1554560248899219457/QAUc2RLt.jpg'
    },
    {
        name: 'IDGAMEFI 🇮🇩',
        img: 'https://pbs.twimg.com/profile_images/1637652651952455680/ORRBGzBZ.jpg'
    },
    {
        name: 'SolApps 🐧',
        img: 'https://pbs.twimg.com/profile_images/1550617169762942980/k0-aFkch.jpg'
    }

    ,
    {
        name: 'Solaris Network',
        img: 'https://pbs.twimg.com/profile_images/1671793652161728514/3OSLH5hT.jpg'
    },
    {
        name: 'AFFLUX',
        img: 'https://pbs.twimg.com/profile_images/1598601684103856128/8mJxW-rv.jpg'
    }
]