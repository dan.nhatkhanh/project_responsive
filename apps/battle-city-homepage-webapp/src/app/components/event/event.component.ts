import { Component, OnInit } from '@angular/core';
import { LINK } from '@wol/shared';

@Component({
  selector: 'wol-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {
  LEGENDS_OF_TANK = LINK.LEGENDS_OF_TANK;
  currentPage = 1;
  bannerName = 1;
  constructor() { }

  ngOnInit(): void {
  }

  href() {
    location.href = this.LEGENDS_OF_TANK;
  }

  openLink() {
    window.open(this.LEGENDS_OF_TANK)
  }

  next() {
    this.currentPage = this.currentPage === 1 ? 2 : 1;
    this.bannerName = this.bannerName < 6 ? this.bannerName + 1 : 1;
  }

}
