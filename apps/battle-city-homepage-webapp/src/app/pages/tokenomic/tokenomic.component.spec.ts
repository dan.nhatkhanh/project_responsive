import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenomicComponent } from './tokenomic.component';

describe('TokenomicComponent', () => {
  let component: TokenomicComponent;
  let fixture: ComponentFixture<TokenomicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TokenomicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenomicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
