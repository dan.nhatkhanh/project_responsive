import { Injectable } from '@angular/core';
import { BaseService } from '@wol/ui';

@Injectable({
  providedIn: 'root'
})
export class CoinService extends BaseService {

  async getCoinInfo() {
    return await this.http.get('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=world-of-legends').toPromise();
  }

  async getCoinPriceInBNB(){
    return await this.http.get('https://api.coingecko.com/api/v3/simple/token_price/binance-smart-chain?contract_addresses=0x5eeb28b5aef44b6664b342d23b1aadce84196386&vs_currencies=bnb,usd').toPromise();
  }
}
