import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { Router } from "@angular/router";
import { KEYS, LINK } from "@wol/shared";
import { environment } from "../environments/environment";
import { WolBaseComponent, BaseService } from "@wol/ui";
import { EnvironmentService } from "@wol/ui";
@Component({
  selector: "wol-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent
  extends WolBaseComponent
  implements OnInit, AfterViewInit
{
  MARKETPLACE = environment.COMMON.MARKETPLACE;
  TIME_TO_END = environment.PRIVATE_SALE.END;
  WHITEPAPER = LINK.WHITEPAPER;
  WIKI = LINK.WIKI;
  NEWS = LINK.NEWS;
  opened = false;
  isAppLoading = true;
  pageKey = 0;
  privateSaleDisplay = false;
  announcementDisplay = true;
  currentUrl = "";
  isOpenPopup = true;
  @ViewChild("popup")
  popup!: ElementRef;

  constructor(
    public router: Router,
    baseService: BaseService,
    _env: EnvironmentService,
    private renderer: Renderer2
  ) {
    super(baseService, _env);
    /**
     * This events get called by all clicks on the page
     */
    // this.renderer.listen('window', 'click', (e: Event) => {
    //   /**
    //    * Only run when toggleButton is not clicked
    //    * If we don't check this, all clicks (even on the toggle button) gets into this
    //    * section which in the result we might never see the menu open!
    //    * And the menu itself is checked here, and it's where we check just outside of
    //    * the menu and button the condition abbove must close the menu
    //    */
    //   if (e.target !== this.popup.nativeElement) {
    //     this.isOpenPopup = false;
    //   }
    // });

    const localStorageLanguages = localStorage.getItem("language");
    if (!localStorageLanguages) {
      localStorage.setItem("language", "en_US");
    }
    this.currentUrl = location.href;
    if ((this.currentUrl || "").includes(KEYS.TOKENOMIC)) {
      this.pageKey = 2;
    }
  }

  @HostListener("document:keydown.escape", ["$event"]) onKeydownHandler(
    event: KeyboardEvent
  ) {
    this.isOpenPopup = false;
  }

  isFullyInView(element: HTMLElement) {
    const rect = element.getBoundingClientRect();
    return rect.top >= 0 && rect.bottom <= window.innerHeight;
  }

  // Function to prevent default scroll behavior
  preventScroll(e: WheelEvent) {
    e.preventDefault();
  }

  preventTab(e: KeyboardEvent) {
    if (e.key === "Tab") {
      e.preventDefault();
    }
  }

  ngAfterViewInit(): void {
    console.log(
      'this.isFullyInView(document.getElementById("background"): ',
      this.isFullyInView(document.getElementById("background")!)
    );
    setTimeout(() => {
      if (this.isFullyInView(document.getElementById("background")!)) {
        window.addEventListener("wheel", this.preventScroll, {
          passive: false,
        });
        window.addEventListener("keydown", this.preventTab, {
          passive: false,
        });
      } else {
        window.removeEventListener("wheel", this.preventScroll);
        window.removeEventListener("keydown", this.preventTab);
      }
    }, 100);
    const body = document.querySelector("body");

    body?.addEventListener("wheel", (e) => {
      const parent = document.getElementById("background");
      if (this.isFullyInView(parent!)) {
        const child = document.getElementById("backgroundVideo");

        const zoomSpeed = 0.02; // Adjust the zoom speed

        // Calculate the maxScale based on the parent and child dimensions
        const parentHeight = Number(parent?.clientHeight);
        const childHeight = Number(child?.clientHeight);
        const maxScale = parentHeight / childHeight; // Maximum zoom level based on height
        const minScale = 0.6; // Minimum zoom level

        // Get the current scale of the child
        let scale =
          parseFloat(getComputedStyle(child!).transform.split(",")[3]) || 1;

        // Calculate the new scale based on scroll direction
        if (e.deltaY > 0) {
          scale += zoomSpeed;
        } else {
          if (this.isFullyInView(parent!)) {
            scale -= zoomSpeed;
          }
        }

        // Ensure the scale stays within the specified bounds
        if (scale > maxScale) scale = maxScale;
        if (scale < minScale) scale = minScale;

        // Apply the new scale to the child element
        (
          document.getElementById("backgroundVideo") as HTMLElement
        ).style.transform = `translate(-50%, -50%) scale(${scale})`;

        // Get the bounding rectangles of parent and child
        const parentRect = parent?.getBoundingClientRect();
        const childRect = child?.getBoundingClientRect();

        const opacitySpeed = 0.02;
        let currentOpacity = 0;

        if (Number((childRect?.height! / 1000).toFixed(1)) >= 0.6) {
          if (e.deltaY > 0) {
            currentOpacity += opacitySpeed;
          } else {
            currentOpacity -= opacitySpeed;
          }

          (document.getElementById("text123") as HTMLElement).style.opacity =
            String(childRect?.height! / 1000);
        } else if (Number((childRect?.height! / 1000).toFixed(1)) < 0.6) {
          (document.getElementById("text123") as HTMLElement).style.opacity =
            "0";
        }

        // Ensure the child does not zoom out of the parent's boundaries
        if (childRect?.height! > parentRect?.height!) {
          (
            document.getElementById("backgroundVideo") as HTMLElement
          ).style.transform = `scale(${scale})`; // Apply corrected scale
        }

        if (
          Number(parentRect?.height.toFixed(0)) ===
          Math.ceil(child?.getBoundingClientRect().height!)
        ) {
          window.removeEventListener("wheel", this.preventScroll);
          window.removeEventListener("keydown", this.preventTab);
        } else {
          window.addEventListener("wheel", this.preventScroll, {
            passive: false,
          });
          window.addEventListener("keydown", this.preventTab, {
            passive: false,
          });
        }
      }
    });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.isAppLoading = false;
      setTimeout(() => {
        if (!(this.currentUrl || "").includes(KEYS.PRIVATE_SALE)) {
          const now = new Date().getTime();
          const distance = this.TIME_TO_END - now;
          if (distance > 0) {
            this.privateSaleDisplay = true;
          }
        }
        // this.announcementDisplay = true;
      }, 500);
    }, 1500);
    // this.intergreateSpaceId();
    // this.reverseResolution();
  }

  closeDialog(event: boolean) {
    if (event) {
      this.privateSaleDisplay = false;
      this.announcementDisplay = false;
    }
  }

  toogle(event: boolean): void {
    this.opened = event;
  }

  onClosed() {
    this.opened = false;
  }

  navigate(key: number): void {
    this.opened = false;
    switch (key) {
      case 0:
        this.pageKey = 0;
        this.router.navigate(["/"]);
        this.scrollTop();
        break;
      case 1:
        this.pageKey = 1;
        break;
      case 2:
        this.pageKey = 2;
        this.router.navigate(["/tokenomic"]);
        this.scrollTop();
        break;
      case 3:
        location.href = this.MARKETPLACE;
        this.pageKey = 3;
        break;
      case 4:
        location.href = this.NEWS;
        this.pageKey = 4;
        break;
    }
  }
  scrollTop() {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "auto",
    });
  }

  // async intergreateSpaceId() {
  //   let name = environment.SPACE_ID_BNB_NAME;
  //   const SID = require('@siddomains/sidjs').default
  //   const SIDfunctions = require('@siddomains/sidjs')
  //   const Web3 = require('web3')

  //   let sid

  //   const rpc = environment.BSC_PROVIDER_URL
  //   const provider = new Web3.providers.HttpProvider(rpc)
  //   const chainId = environment.CHAIN_ID
  //   sid = new SID({ provider, sidAddress: SIDfunctions.getSidAddress(chainId) })

  //   const address = await sid.name(name).getAddress() // 0x123
  //   console.log("name: %s, address: %s", name, address)
  // }

  // async reverseResolution() {
  //   const SID = require('@siddomains/sidjs').default
  //   const SIDfunctions = require('@siddomains/sidjs')
  //   const Web3 = require('web3')
  //   let address = environment.SPACE_ID_BNB_ADDRESS;

  //   let sid
  //   const rpc = environment.BSC_PROVIDER_URL
  //   const provider = new Web3.providers.HttpProvider(rpc)
  //   const chainId = environment.CHAIN_ID
  //   sid = new SID({ provider, sidAddress: SIDfunctions.getSidAddress(chainId) })

  //   const sidName = await sid.getName(address)
  //   console.log("name: %s, address: %s", sidName?.name, address)
  // }
}
