export const environment = {
  production: true,
  isProduction: true,
  COMMON: {
    LANG_DEFAULT: 'en_US',
  },
  BASE_URL: 'https://api.battlecity.io',
  SYSTEM_DEFAULT: 'battle_city',
};
