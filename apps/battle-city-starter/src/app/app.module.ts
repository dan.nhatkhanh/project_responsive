import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';
import { baseReducers, UiModule, WolButtonModule, WolCardAssetModule, WolDatepickerModule, WolTableModule, WolTabsModule } from '@wol/ui';
import { WolChipModule } from 'libs/ui/src/lib/components/wol-chip/wol-chip.module';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ComponentsPage } from './pages/components/components.component';

@NgModule({
  declarations: [
    AppComponent,
    ComponentsPage
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    ReactiveFormsModule,
    UiModule.forRoot(environment),
    WolCardAssetModule,
    WolButtonModule,
    WolChipModule,
    WolTabsModule,
    WolDatepickerModule,
    WolTableModule,
    AppRoutingModule,
    StoreModule.forRoot({ ...baseReducers }, { initialState: {} }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
