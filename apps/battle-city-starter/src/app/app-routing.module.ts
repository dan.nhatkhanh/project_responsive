import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsPage } from './pages/components/components.component';


export const devRoutes: Routes = [
  {
    path: '',
    component: ComponentsPage,
    data: {
      screen: 'Components Page',
    },
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(devRoutes, {
  })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
