import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CardItem } from '@wol/ui';

@Component({
    selector: 'wol-components',
    templateUrl: `./components.component.html`,
    styleUrls: [`./components.component.scss`]
})

export class ComponentsPage implements OnInit {
    constructor() { }

    table_data_keys = ['header1', 'header2', 'header3'];
    table_data_source = [
        {
            header1: '1',
            header2: 'US',
            header3: '$8888'
        },
        {
            header1: '2',
            header2: 'SGD',
            header3: '$9999'
        }
        ,
        {
            header1: '3',
            header2: 'RUP',
            header3: '$7777'
        }
    ]
    table_title: FormControl = new FormControl('Table Title');
    tabSelectContent = 'Tab1';
    dateObject = {};
    list_tab = [
        {
            id: 'tab1',
            text: 'Tab1'
        },
        {
            id: 'tab2',
            text: 'Tab2'
        },
        {
            id: 'tab3',
            text: 'Tab3'
        },
        {
            id: 'tab4',
            text: 'Tab4'
        }

    ]

    list_chip = [
        {
            text: 'battle_city'
        },
        {
            text: 'battle_city_mu'
        },
        {
            text: 'battle_city_tank'
        }
    ]

    list_button = [
        {
            text: 'Buy now',
            background: 'rgb(255, 54, 211)',
            color: '#fff',
            margin: 21
        },
        {
            text: 'Open',
            background: 'rgb(57, 255, 54)',
            color: '#333',
            margin: 21
        },
        {
            text: 'Buy now',
            background: 'rgb(150, 54,255)',
            color: '#fff',
            margin: 21
        },
        {
            text: 'Open',
            background: '#668725',
            color: '#fff',
            margin: 21
        }
    ]

    list_card: CardItem[] = [
        {
            id: 1,
            asset_id: 1,
            name: 'Starter Box',
            price: 10,
            type: {
                key: '1',
                value: 'Starter'
            },
            image: 'https://portal.dev.battlecity.io/assets/images/chest@2x.png',
            view: 55,
            category: {
                key: '1',
                value: 'Box'
            },
            medal: 'S'
        },
        {
            id: 1,
            asset_id: 1,
            name: 'Starter Egg',
            price: 10,
            type: {
                key: '1',
                value: 'Starter'
            },
            image: 'https://portal.dev.battlecity.io/assets/images/egg.png',
            view: 56,
            category: {
                key: '1',
                value: 'Egg'
            },
            medal: 'S'
        },
        {
            id: 1,
            asset_id: 1,
            name: 'Hero 1',
            price: 10,
            type: {
                key: '1',
                value: 'Dark'
            },
            image: 'https://portal.dev.battlecity.io/assets/images/dark_night_hero.png',
            view: 56,
            category: {
                key: '1',
                value: 'Hero'
            },
            medal: 'S'
        },
        {
            id: 1,
            asset_id: 1,
            name: 'Hero 2',
            price: 10,
            type: {
                key: '1',
                value: 'Dark'
            },
            image: 'https://portal.dev.battlecity.io/assets/images/hero-2.png',
            view: 56,
            category: {
                key: '1',
                value: 'Hero'
            },
            medal: 'S'
        }
    ]

    ngOnInit() { }

    listenTab(tab: any) {
        console.log(tab)
        this.tabSelectContent = tab?.value?.text;
    }

    dateChange(value: any) {
        console.log(value)
        this.dateObject = value;
    }
}