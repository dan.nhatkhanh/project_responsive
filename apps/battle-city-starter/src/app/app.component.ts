import { Component } from '@angular/core';

@Component({
  selector: 'wol-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'battle-city-starter';
  
}
